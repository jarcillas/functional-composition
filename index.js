// Function Composition

const raiseToTwo = (a) => a ** 2;
const addFive = (a) => a + 5;
const subtractTwo = (a) => a - 2;
const compose = (a, b) => (c) => a(b(c));

console.log('Function Composition Examples:');
console.log(compose(raiseToTwo, addFive)(3)); // (3 + 5) ** 2 = 64
console.log(compose(addFive, raiseToTwo)(5)); // (5 ** 2) + 5 = 30
console.log(compose(subtractTwo, raiseToTwo)(4)); // (4 ** 2) - 2 = 14

// Currying

function multiply(x) {
  return function (y) {
    return x * y;
  };
}

const multiply5 = multiply(5); // multiply5 becomes a function that multiplies its parameter by 5

console.log('Currying Example:');
console.log('multiply(2)(3) returns:');
console.log(multiply(2)(3));
console.log('multiply5(7) returns:');
console.log(multiply5(7));
